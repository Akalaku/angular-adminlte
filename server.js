var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var authApi = require('./server/auth.js');
var userApi = require('./server/user.js');

var app = express();
app.use(morgan('combined')); //logger
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var router = express.Router();
router.get('/user', userApi.get);
router.use('/auth', authApi);
app.use('/api', router);

//app.use('/api', require('./server/api.js'))

app.use('*', function (req, res) {
    res.end("404 not found...");
})

const server = app.listen(8081, function () {
    const port = server.address().port;
    console.log("Server is running... at %s", port);
})