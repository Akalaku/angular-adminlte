const express = require('express');
const router = express.Router();
var oracledb = require('oracledb');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var config = require('./config');
var ldap = require('ldapjs');

module.exports = router;

router.get('/show', function (req, res) {
    res.end("Hi,show api success");
})

router.get('/ldap', function (req, res) {

    const ldap = require('ldapjs');
    const assert = require('assert');

    // LDAP Connection Settings
    const server = "true.th:389"; // 192.168.1.1
    const principalName = "tanin5@true.th"; // Username
    const userPrincipalName = "eakkala@true.th"; // Username
    const password = "Akalaku101"; // User password
    const adSuffix = "dc=true,dc=th"; // test.com

    // Create client and bind to AD
    const client = ldap.createClient({
        url: `ldap://${server}`
    });

    client.bind(userPrincipalName, password, err => {
        if (err) {
            console.log(err.message);
        }
    });
    console.log("err.message");
    // Search AD for user
    const searchOptions = {
        scope: "sub",
        filter: `(userPrincipalName=${principalName})`
    };

    client.search(adSuffix, searchOptions, (err, res) => {
        assert.ifError(err);

        res.on('searchEntry', entry => {
            console.log(entry.object.name);
        });
        res.on('searchReference', referral => {
            console.log('referral: ' + referral.uris.join());
        });
        res.on('error', err => {
            //console.error('error: ' + err.message);
        });
        res.on('end', result => {
            //console.log(result);
        });
    });

    // Wrap up
    client.unbind(err => {
        assert.ifError(err);
    });
    res.end("Hi,show2 api success");
})

router.post('/login', function (req, res, next) {
    var user = {
        username: req.body.username
    };
    var unhashedPassword = req.body.password;

    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
        }

        bcrypt.hash(unhashedPassword, salt, function (err, hash) {
            if (err) {
                return next(err);
            }

            user.hashedPassword = hash;

            //insertUser(user, function(err, user) {
            var payload;

            //if (err) {
            //    return next(err);
            //}

            payload = {
                username: user.username
            };

            res.status(200).json({
                username: user.username,
                token: jwt.sign(payload, config.jwtSecretKey, { expiresIn: "1h" })
            });
            //});
        });
    });
})



//module.exports.post = post;

function insertUser(user, cb) {
    oracledb.getConnection(
        config.database,
        function (err, connection) {
            if (err) {
                return cb(err);
            }

            connection.execute(
                'insert into jsao_users ( ' +
                '   email, ' +
                '   password, ' +
                '   role ' +
                ') ' +
                'values (' +
                '    :email, ' +
                '    :password, ' +
                '    \'BASE\' ' +
                ') ' +
                'returning ' +
                '   id, ' +
                '   email, ' +
                '   role ' +
                'into ' +
                '   :rid, ' +
                '   :remail, ' +
                '   :rrole',
                {
                    email: user.email.toLowerCase(),
                    password: user.hashedPassword,
                    rid: {
                        type: oracledb.NUMBER,
                        dir: oracledb.BIND_OUT
                    },
                    remail: {
                        type: oracledb.STRING,
                        dir: oracledb.BIND_OUT
                    },
                    rrole: {
                        type: oracledb.STRING,
                        dir: oracledb.BIND_OUT
                    }

                },
                {
                    autoCommit: true
                },
                function (err, results) {
                    if (err) {
                        connection.release(function (err) {
                            if (err) {
                                console.error(err.message);
                            }
                        });

                        return cb(err);
                    }

                    cb(null, {
                        id: results.outBinds.rid[0],
                        email: results.outBinds.remail[0],
                        role: results.outBinds.rrole[0]
                    });

                    connection.release(function (err) {
                        if (err) {
                            console.error(err.message);
                        }
                    });
                });
        }
    );
}