import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void{
    const script = document.createElement('script'); 
    script.src = 'assets/js/chart.js';
    document.body.appendChild(script);

    script.setAttribute("id","chart");
    script.setAttribute("data-dummy-json",[30,50,42,21,88,30,90].toString());
    script.setAttribute("data-dummy2-json",[20,40,32,11,78,20,80].toString());
  }

}
