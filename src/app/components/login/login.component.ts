import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mUsername:string = "";
  mPassword:string = "";
  private generic:IGenericObj;

  constructor(private router:Router,private dataSrv:DataService) { }

  ngOnInit() {
    this.dataSrv.getGeneric().subscribe((response) =>{
      this.generic = response;    
      console.log(this.generic.base_url);
    })
  }

  onClickSubmit(){
    if(this.mUsername == "eakkala"){
      this.router.navigate(["/dashboard"]);
    }else{
      window.alert("Fails");
    }
  }

  
}

interface IGenericObj {
  status_code: number;
  status_description: string;
  base_url: string;
  show_getty_image_button: boolean;
  show_other_web_button: boolean;
  other_web_button: Otherwebbutton[];
}

interface Otherwebbutton {
  id: string;
  title: string;
  url: string;
  image: string;
  show: boolean;
}