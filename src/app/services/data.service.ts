import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: Http) { }

  getGeneric() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('cache-control', 'no-cache');
    let options = new RequestOptions({ headers: headers });
    let url = "https://bsddev.truecorp.co.th/aalib/api/v1/generic";
    return this.http.get(url, options)
      .map(res => res.json());
  }

  getProfile() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `Bearer ${authToken}`);

    return this.http
      .get('/profile', { headers })
      .map(res => res.json());
  }
}
